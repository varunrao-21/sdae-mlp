#!/bin/sh

cd ..

wget https://archive.ics.uci.edu/ml/machine-learning-databases/00310/UJIndoorLoc.zip

mkdir data
unzip UJIndoorLoc.zip -d data
mv data/UJIndoorLoc/*.csv data/

rm -rf data/UJIndoorLoc/
rm *.zip