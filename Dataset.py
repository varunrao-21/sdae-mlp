import numpy as np
import torch
from torch.utils.data import Dataset

class IndoorLocDataset(Dataset):
    def __init__(self, file_name):
        #TODO: Include preprocessing here.
        data = np.loadtxt(file_name, delimiter = ",")
        self.rssi = data[:,:520]
        self.loc = data[:, 520:]

    def __len__(self):
        return self.rssi.shape[0]
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        sample = {
            'x': self.rssi[idx],
            'y': self.loc[idx]
        }

        return sample