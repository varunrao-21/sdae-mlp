def trainDAE_Epoch(dae, loader, loss_func, optimizer, device, dae1 = None, dae2 = None):
    dae.train()
    if dae1:
        dae1.train()
    if dae2:
        dae2.train()
    totalLoss = 0.0
    for _, batch in enumerate(loader):
        optimizer.zero_grad()

        actual_inputs = batch['x']

        actual_inputs = actual_inputs.float()
        actual_inputs = actual_inputs.to(device)
        
        inputs = actual_inputs
        if dae1:
            inputs = dae1.encode(inputs)
        if dae2:
            inputs = dae2.encode(inputs)

        recon_inputs = dae.forward(inputs)

        if dae2:
            recon_inputs = dae2.decode(recon_inputs)
        if dae1:
            recon_inputs = dae1.decode(recon_inputs)

        loss = loss_func(recon_inputs, actual_inputs)
        totalLoss += loss.item()

        loss.backward()
        optimizer.step()
    return totalLoss / len(loader)

def trainMLP_Epoch(model, loader, loss_func, optimizer, sdae, device):
    '''
    Trains an epoch on the model.

    Args:
        model: A torch.nn.Module object which will be trained.
        loader: A torch.utils.data.Loader object which will generate batches from the data.
        loss_func: A torch.nn object which will be used for calculation of loss.
        optimizer: A torch.optim object which will do the back-propagation of loss through the model.
        sdae: A models.SDAE.SDAE object which is the trained Stacked Denoising Autoencoder.
        device: torch.device which tells model to be trained on cpu or gpu.

    Returns:
        A float value which is the average of loss across all batches in the epoch.
    '''
    model.train()
    totalLoss = 0.0
    for _, batch in enumerate(loader):
        optimizer.zero_grad()

        inputs, coords = batch['x'], batch['y']

        inputs = inputs.float()
        coords = coords.float()

        inputs = inputs.to(device)
        coords = coords.to(device)

        denoised_inputs = sdae.denoise(inputs)

        # Forward pass through the model
        predicted_coords = model.forward(denoised_inputs)

        loss = loss_func(predicted_coords, coords)
        totalLoss += loss.item()

        loss.backward()
        optimizer.step()
    return totalLoss / len(loader)