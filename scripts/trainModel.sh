#!/bin/sh

LOG_DIR=logs/MLP_training
SDAE_PATH=saved/SDAE.pth

cd ..

if [ ! -d "data" ]; then
    echo "Data folder not found! Cannot train without data."
    exit
fi

if [ -d "$LOG_DIR" ]; then
    rm -rf "$LOG_DIR"
fi

echo "Previous log files deleted."

if [ ! -f "$SDAE_PATH" ]; then
    echo "SDAE Model not found!"
    exit
fi

python3 train_model.py