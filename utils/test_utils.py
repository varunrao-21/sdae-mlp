import torch

def testDAE_Epoch(dae, loader, loss_func, device, dae1 = None, dae2 = None):
    dae.eval()
    if dae1:
        dae1.eval()
    if dae2:
        dae2.eval()
    test_loss = 0.0
    with torch.no_grad():
        for _, batch in enumerate(loader):
            actual_inputs = batch['x']
            actual_inputs = actual_inputs.float()
            actual_inputs = actual_inputs.to(device)

            inputs = actual_inputs
            if dae1:
                inputs = dae1.encode(inputs)
            if dae2:
                inputs = dae2.encode(inputs)
            
            recon_inputs = dae.forward(inputs)

            if dae2:
                recon_inputs = dae2.decode(recon_inputs)
            if dae1:
                recon_inputs = dae1.decode(recon_inputs)

            test_loss += loss_func(recon_inputs, actual_inputs).item()
    return test_loss / len(loader)

def testMLP_Epoch(model, loader, loss_func, sdae, device):
    model.eval()
    test_loss = 0.0
    with torch.no_grad():
        for _, batch in enumerate(loader):
            inputs, coords = batch['x'], batch['y']

            inputs = inputs.float()
            coords = coords.float()

            inputs = inputs.to(device)
            coords = coords.to(device)

            denoised_inputs = sdae.denoise(inputs)

            predicted_coords = model.forward(denoised_inputs)

            test_loss += loss_func(predicted_coords, coords).item()
    return test_loss / len(loader)
