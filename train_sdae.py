import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

from torchsummary import summary

import json

from utils.param_utils import *
from utils.data_utils import initData
from utils.test_utils import testDAE_Epoch
from models.DAE import DAE
from trainer import trainDAE_Epoch

torch.manual_seed(7)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def main():
    global device
    CONFIG_FILE = "sdae-config.json"
    params = readParams(CONFIG_FILE)
    
    writer = SummaryWriter(params['trainer']['log_dir']+"/"+"SDAE_training") # Tensorboard logging.

    if params['verbose']:
        print("################## Parameters ##################")
        print(json.dumps(params, indent = 2))
    
    # Load data
    train, val = initData(params['data_loader'])
    trainSet, trainLoader = train
    valSet, valLoader = val

    if params['verbose']:
        print("\n")
        print("##################### Data #####################")
        print("Data imported from {}".format(params['data_loader']['data_file']))
        print("Samples to be used for training: {}".format(len(trainSet)))
        print("Samples to be used for validating: {}".format(len(valSet)))

    dae1 = DAE(0.4, 520, 256)
    dae2 = DAE(0.5, 256, 128)
    dae3 = DAE(0.6, 128, 64)

    # Train the first dae now
    dae1.to(device)
    if params['verbose']:
        print("\n")
        print("##################### DAE--1 ####################")
        summary(dae1, input_size = (1, 520))
    
    loss = nn.MSELoss()
    opt1 = optim.Adam(dae1.parameters(), lr = params['optimizer']['lr'])

    # Start training
    print("\n")
    print("Training over {0} batches, validating over {1} batches".format(len(trainLoader), len(valLoader)))
    print("Training for {0} epochs".format(params['trainer']['epochs'][0]))
    if not params['verbose']:
        print("WARNING: Verbose turned off. Logging only on tensorboard now.")

    for epoch in range((params['trainer']['epochs'][0])):
        trainLoss = trainDAE_Epoch(dae1, trainLoader, loss, opt1, device)
        valLoss = testDAE_Epoch(dae1, trainLoader, loss, device)
        if params['verbose']:
            print("Epoch {0} ----> Training loss: {1:.4f} \t Validation loss: {2:.4f}".format(epoch + 1,
                                                                                              trainLoss,
                                                                                              valLoss))
        writer.add_scalar('DAE--1/Loss/Train', trainLoss, epoch + 1)
        writer.add_scalar('DAE--1/Loss/Validation', valLoss, epoch + 1)

    print("DAE--1 trained. Starting training for DAE--2 now...")
    
    # Train the second dae now
    dae2.to(device)
    
    if params['verbose']:
        print("\n")
        print("##################### DAE--2 ####################")
        summary(dae2, input_size = (1, 256))
    
    loss = nn.MSELoss()
    opt_params = list(dae1.parameters()) + list(dae2.parameters())
    opt2 = optim.Adam(opt_params, lr = params['optimizer']['lr'])

    # Start training
    print("\n")
    print("Training over {0} batches, validating over {1} batches".format(len(trainLoader), len(valLoader)))
    print("Training for {0} epochs".format(params['trainer']['epochs'][1]))
    if not params['verbose']:
        print("WARNING: Verbose turned off. Logging only on tensorboard now.")

    for epoch in range((params['trainer']['epochs'][1])):
        trainLoss = trainDAE_Epoch(dae2, trainLoader, loss, opt2, device, dae1 = dae1)
        valLoss = testDAE_Epoch(dae2, trainLoader, loss, device, dae1 = dae1)
        if params['verbose']:
            print("Epoch {0} ----> Training loss: {1:.4f} \t Validation loss: {2:.4f}".format(epoch + 1,
                                                                                              trainLoss,
                                                                                              valLoss))
        writer.add_scalar('DAE--2/Loss/Train', trainLoss, epoch + 1)
        writer.add_scalar('DAE--2/Loss/Validation', valLoss, epoch + 1)

    print("DAE--2 trained. Starting training for DAE--3 now...")
    
    # Train the third dae now
    # Also update two DAE weights trained before
    dae3.to(device)
    if params['verbose']:
        print("\n")
        print("##################### DAE--3 ####################")
        summary(dae3, input_size = (1, 128))
    
    loss = nn.MSELoss()
    opt_params = list(dae1.parameters()) + list(dae2.parameters()) + list(dae3.parameters())
    opt3 = optim.Adam(opt_params, lr = params['optimizer']['lr'])

    # Start training
    print("\n")
    print("Training over {0} batches, validating over {1} batches".format(len(trainLoader), len(valLoader)))
    print("Training for {0} epochs".format(params['trainer']['epochs'][2]))
    if not params['verbose']:
        print("WARNING: Verbose turned off. Logging only on tensorboard now.")

    for epoch in range((params['trainer']['epochs'][2])):
        trainLoss = trainDAE_Epoch(dae3, trainLoader, loss, opt3, device, dae1 = dae1, dae2 = dae2)
        valLoss = testDAE_Epoch(dae3, trainLoader, loss, device, dae1 = dae1, dae2 = dae2)
        if params['verbose']:
            print("Epoch {0} ----> Training loss: {1:.4f} \t Validation loss: {2:.4f}".format(epoch + 1,
                                                                                              trainLoss,
                                                                                              valLoss))
        writer.add_scalar('DAE--3/Loss/Train', trainLoss, epoch + 1)
        writer.add_scalar('DAE--3/Loss/Validation', valLoss, epoch + 1)

    print("All DAEs trained!")

    torch.save({
        'dae1_state_dict': dae1.state_dict(),
        'dae2_state_dict': dae2.state_dict(),
        'dae3_state_dict': dae3.state_dict()
    }, "{}/SDAE.pth".format(params['trainer']['save_dir']))

    print("Saved at {}/SDAE.pth".format(params['trainer']['save_dir']))
    writer.close()

if __name__ == "__main__":
    main()