import torch.nn as nn
import torch.nn.functional as func

class LocatorNet(nn.Module):
    def __init__(self):
        super(LocatorNet, self).__init__()
        self.clf = nn.Sequential(
            # Dense 1
            nn.Linear(64, 128),
            nn.Tanh(),
            nn.BatchNorm1d(128),
            # Dense 2
            nn.Linear(128, 256),
            nn.Tanh(),
            nn.BatchNorm1d(256),
            # Dense 3
            nn.Linear(256, 512),
            nn.Tanh(),
            nn.BatchNorm1d(512),
            # Dense 4
            nn.Linear(512, 256),
            nn.Tanh(),
            nn.BatchNorm1d(256),
            # Dense 5
            nn.Linear(256, 128),
            nn.Tanh(),
            nn.BatchNorm1d(128),
            # Dense 6
            nn.Linear(128, 64),
            nn.Tanh(),
            nn.BatchNorm1d(64),
            # Dense 7
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.BatchNorm1d(32),
            # Dense 8
            nn.Linear(32, 16),
            nn.Tanh(),
            nn.BatchNorm1d(16),
            # Dense 9
            nn.Linear(16, 8),
            nn.Tanh(),
            nn.BatchNorm1d(8),
            # Final output
            nn.Linear(8, 2),
            nn.Sigmoid()
        )

    def forward(self, x):
        return self.clf(x)