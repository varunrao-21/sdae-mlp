from torch.utils.data import random_split, DataLoader

from Dataset import IndoorLocDataset

def __splitDataset(dataset, dataParams):
    totalLen = len(dataset)
    
    valLen = int(len(dataset) * dataParams['validation_split'])
    trainLen = totalLen - valLen

    return random_split(dataset, [trainLen, valLen])

def __loadDataset(dataParams):
    dataset = IndoorLocDataset(dataParams['data_file'])
    return dataset

def __createLoader(dataset, dataParams):
    return DataLoader(dataset,
                      batch_size = dataParams['batch_size'],
                      shuffle = dataParams['shuffle'],
                      num_workers = dataParams['num_workers'])

def initData(dataParams):
    dataset = __loadDataset(dataParams)
    
    trainDataset, valDataset = __splitDataset(dataset, dataParams)
    
    trainLoader = __createLoader(trainDataset, dataParams)
    valLoader = __createLoader(valDataset, dataParams)

    return ((trainDataset, trainLoader), (valDataset, valLoader))

def initTestData(dataFile):
    dataset = IndoorLocDataset(dataFile)
    dataloader =  DataLoader(dataset,
                             batch_size = 1,
                             shuffle = False,
                             num_workers = 6)
    return dataset, dataloader