# Indoor Localization using Stacked Denoising Autoencoder and MLP

This is an implementation of the paper [A Robust Wi-Fi Fingerprint Positioning Algorithm Using Stacked Denoising Autoencoder and Multi-Layer Perceptron](https://www.mdpi.com/2072-4292/11/11/1293). 

# Set-up

## Data

Use this to get the UJIndoorLoc dataset:

``` bash
cd scripts/
./getData.sh
```

## Preprocessing

Use the `preprocess.py` file from the `preprocessing/` folder to preprocess the data. More specifically, the RSSIs and the latitude and longitude are scaled using the min and max.

``` bash
python3 preprocess.py
```

That's it for the set-up. We are now ready to train the model!

# Training

## Training the SDAE.

``` bash
cd scripts/
./trainSDAE.sh
```

For tensorboard logging (on other terminal):
```bash
cd ../
tensorboard --logdir=logs/SDAE_training
```

## Training the model.

``` bash
cd scripts/
./trainModel.sh
```

For tensorboard logging (on other terminal):
```bash
cd ../
tensorboard --logdir=logs/MLP_training
```