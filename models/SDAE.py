import torch
from models.DAE import DAE

class SDAE:
    def __init__(self, model_file, device):
        self.dae1 = DAE(0.4, 520, 256)
        self.dae1.to(device)
        self.dae2 = DAE(0.5, 256, 128)
        self.dae2.to(device)
        self.dae3 = DAE(0.6, 128, 64)
        self.dae3.to(device)

        checkpoint = torch.load(model_file)
        self.dae1.load_state_dict(checkpoint['dae1_state_dict'])
        self.dae2.load_state_dict(checkpoint['dae2_state_dict'])
        self.dae3.load_state_dict(checkpoint['dae3_state_dict'])

        self.dae1.eval()
        self.dae2.eval()
        self.dae3.eval()

    def denoise(self, x):
        x = self.dae1.encode(x)
        x = self.dae2.encode(x)
        x = self.dae3.encode(x)
        return x.detach()