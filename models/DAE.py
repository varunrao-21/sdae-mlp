import torch.nn as nn
import torch.nn.functional as func

class DAE(nn.Module):
    def __init__(self, noising_factor, in_features, hidden_neurons):
        '''
        Denoising Auto-encoder class. 

        Attributes:
            noising_factor: Float value between 0 and 1 indicating noise to be added.
            in_features: Int number of input features.
            hidden_neurons: Int number of hidden neurons.
            
            encoder: Can be used to denoise the input.
        '''
        super(DAE, self).__init__()
        self.__noising = nn.Dropout(p = noising_factor)
        self.__encoder = nn.Linear(in_features=in_features, out_features=hidden_neurons)
        self.__decoder = nn.Linear(in_features=hidden_neurons, out_features=in_features)

    def forward(self, x):
        x = self.__noising(x)
        x = func.relu(self.__encoder(x))
        return self.__decoder(x)
    
    def encode(self, x):
        if self.training:
            x = self.__noising(x)
        return func.relu(self.__encoder(x))

    def decode(self, x):
        return self.__decoder(x)