import numpy as np
import json

def main():
    TRAINING_FILE = "../data/trainingData.csv"
    TEST_FILE = "../data/validationData.csv"
    
    OP_TRAIN_FILE = "../data/preprocessed-training-data.csv"
    OP_TEST_FILE = "../data/preprocessed-test-data.csv"


    print("Preprocessing data from {}".format(TRAINING_FILE), end = "\r")
    feat_mat = np.genfromtxt(TRAINING_FILE, delimiter = ",", skip_header = 1)
    features = feat_mat[:,:522]
    features[features == 100] = -110
    
    # Normalize RSSI features.
    min_rssi = -110
    max_rssi = 0

    features[:, :520] = (features[:, :520] - min_rssi) / (max_rssi - min_rssi)
    
    # Normalize Longitude and Latitude
    min_long = np.min(features[:,520])
    max_long = np.max(features[:,520])

    features[:,520] = (features[:, 520] - min_long) / (max_long - min_long)

    min_lat = np.min(features[:,521])
    max_lat = np.max(features[:,521])
    features[:,521] = (features[:, 521] - min_lat) / (max_lat - min_lat)

    # Save the 
    targetNormJSON = dict()
    targetNormJSON['latitude'] = {'min_lat': min_lat, 'max_lat': max_lat}
    targetNormJSON['longitude'] = {'min_long': min_long, 'max_long': max_long}

    with open('../target-norm.json', 'w') as fp:
        json.dump(targetNormJSON, fp)

    np.savetxt(OP_TRAIN_FILE, features, fmt = '%10.6f', delimiter = ',')
    
    print("Preprocessed data from {0} stored in {1}".format(TRAINING_FILE, OP_TRAIN_FILE))

    # Preprocess test data
    print("Preprocessing data from {}".format(TEST_FILE), end = "\r")
    feat_mat = np.genfromtxt(TEST_FILE, delimiter = ",", skip_header = 1)
    features = feat_mat[:,:522]
    features[features == 100] = -110

    # Normalize lat and long
    features[:, :520] = (features[:, :520] - min_rssi) / (max_rssi - min_rssi)

    np.savetxt(OP_TEST_FILE, features, fmt = '%10.6f', delimiter = ',')

    print("Preprocessed data from {0} stored in {1}".format(TEST_FILE, OP_TEST_FILE))

if __name__ == "__main__":
    main()