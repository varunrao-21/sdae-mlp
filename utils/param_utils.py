import json

def readParams(paramFilename):
    '''
    Reads parameters from a JSON file.

    Args:
        paramFile: JSON file with parameters

    Returns:
        params: Dict with parameters
    '''
    with open(paramFilename) as paramFile:
        params = json.load(paramFile)
    return params