#!/bin/sh

LOG_DIR=logs/SDAE_training
SAVE_DIR=saved

cd ..

if [ ! -d "data" ]; then
    echo "Data folder not found! Cannot train without data."
    exit
fi

if [ -d "$LOG_DIR" ]; then
    rm -rf "$LOG_DIR"
fi

echo "Previous log files deleted."

if [ ! -d "$SAVE_DIR" ]; then
    mkdir "$SAVE_DIR"
fi

echo "Created folder for model."

python3 train_sdae.py