import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

from torchsummary import summary

import json

from utils.param_utils import *
from utils.data_utils import initData, initTestData
from models.SDAE import SDAE
from models.MLP import LocatorNet
from utils.test_utils import testMLP_Epoch
from trainer import trainMLP_Epoch

torch.manual_seed(42)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def main():
    global device
    CONFIG_FILE = "model-config.json"
    params = readParams(CONFIG_FILE)
    
    writer = SummaryWriter(params['trainer']['log_dir']+"/"+"MLP_training") # Tensorboard logging.

    if params['verbose']:
        print("################## Parameters ##################")
        print(json.dumps(params, indent = 2))
    
    # Load data
    train, val = initData(params['data_loader'])
    trainSet, trainLoader = train
    valSet, valLoader = val

    if params['verbose']:
        print("\n")
        print("##################### Data #####################")
        print("Data imported from {}".format(params['data_loader']['data_file']))
        print("Samples to be used for training: {}".format(len(trainSet)))
        print("Samples to be used for validating: {}".format(len(valSet)))

    # Set up SDAE from the saved model
    sdae = SDAE(params['trainer']['SDAE_model_file'], device)
    print("Initialized Stacked Denoising Autoencoder from {}".format(params['trainer']['SDAE_model_file']))

    # Set up MLP model
    net = LocatorNet()
    net.to(device)
    if params['verbose']:
        print("\n")
        print("##################### Model ####################")
        print(net)

    # Set up loss function and optimizer and scheduler (if applicable)
    loss = nn.MSELoss()
    opt = optim.RMSprop(net.parameters(), lr = params['optimizer']['lr'])
    scheduler = None
    if params['optimizer']['scheduler']:
        scheduler = optim.lr_scheduler.ReduceLROnPlateau(opt,
                                                         "min",
                                                         factor = params['optimizer']['scheduler']['factor'],
                                                         patience = params['optimizer']['scheduler']['patience'],
                                                         threshold = params['optimizer']['scheduler']['tolerance'])

    # Start training
    print("\n")
    print("Training over {0} batches, validating over {1} batches".format(len(trainLoader), len(valLoader)))
    print("Training for {0} epochs".format(params['trainer']['epochs']))
    if not params['verbose']:
        print("WARNING: Verbose turned off. Logging only on tensorboard now.")
    
    for epoch in range(params['trainer']['epochs']):
        trainLoss = trainMLP_Epoch(net, trainLoader, loss, opt, sdae, device)
        valLoss = testMLP_Epoch(net, valLoader, loss, sdae, device)
        if params['verbose']:
            print("Epoch {0} ----> Training loss: {1:.4f} \t Validation loss: {2:.4f}".format(epoch + 1,
                                                                                              trainLoss,
                                                                                              valLoss))
        if scheduler:
            scheduler.step(valLoss)
        writer.add_scalar('Loss/Train', trainLoss, epoch + 1)
        writer.add_scalar('Loss/Validation', valLoss, epoch + 1)

    torch.save(net.state_dict(), "{}/MLP.pth".format(params['trainer']['save_dir']))
    print("\nTrained model saved to {}/MLP.pth".format(params['trainer']['save_dir']))

    writer.close()

if __name__ == "__main__":
    main()